import { request } from "./bowwow.es.js";

document.getElementById( "mocha" ).innerHTML = "";

describe( "bowwow", function bowwowSuite(){
	describe( "#request", function RequestSuite(){
		var valid = {
			"url": "https://www.google.com",
			"method": "get",
			"urlRoot": ""
		};
		var defaultHeaders = {
			"Content-Type": "application/json;charset=UTF-8"
		};
		var emptyResponse = {
			"headers": new Headers(),
			"ok": undefined,
			"redirected": undefined,
			"status": undefined,
			"statusText": undefined,
			"type": undefined,
			"url": undefined
		};
		var rejectedResponse = Object.assign( {}, emptyResponse, {
			"ok": false,
			text(){
				return Promise.resolve( "rejected" );
			}
		} );
		var resolvedResponse = Object.assign( {}, emptyResponse, {
			"ok": true,
			text(){
				return Promise.resolve( "resolved" );
			}
		} );
		var extractedResponse = Object.assign( {}, emptyResponse, { "headers": {} } );

		it( "should default to an empty object and throw if called with nothing", async () => {
			try{
				await request();
			}
			catch( e ){
				e.should.be.an.instanceOf( Error );
			}
		} );

		it( "should throw if no method is supplied", async () => {
			try{
				await request( { "url": "test" } );
			}
			catch( e ){
				e.should.be.an.instanceOf( Error );
			}
		} );

		it( "should throw if no url is supplied", async () => {
			try{
				await request( { "method": "GET" } );
			}
			catch( e ){
				e.should.be.an.instanceOf( Error );
			}
		} );

		describe( "option munging to get a Request object", function GetRequestSuite(){
			var fetchStub;
			var reqStub;
			var headersStub;

			before( () => {
				fetchStub = sinon.stub( window, "fetch" );
				reqStub = sinon.stub( window, "Request" );
				headersStub = sinon.stub( window, "Headers" );
			} );

			beforeEach( () => {
				fetchStub.reset();
				reqStub.reset();
				headersStub.reset();

				fetchStub.rejects();
				headersStub.returnsArg( 0 );
			} );

			describe( "method", function MethodSuite(){
				it( "should always uppercase the request method", () => {
					request( Object.assign( {}, valid ) );

					reqStub.getCall( 0 ).args[ 1 ].method.should.equal( valid.method.toUpperCase() );

					reqStub.resetHistory();

					request( {
						"url": valid.url,
						"method": "stUff"
					} );

					reqStub.getCall( 0 ).args[ 1 ].method.should.equal( "STUFF" );
				} );
			} );

			describe( "url", function UrlSuite(){
				it( "should use the system-provided blank string as the urlRoot if the config `urlRoot` property is undefined or null", () => {
					request( Object.assign( {}, valid, { "urlRoot": null } ) );

					reqStub.getCall( 0 ).args[ 0 ].should.equal( valid.url );
					reqStub.getCall( 0 ).args[ 1 ].url.should.equal( valid.url );

					reqStub.resetHistory();

					request( {
						"url": valid.url,
						"method": valid.method
					} );

					reqStub.getCall( 0 ).args[ 0 ].should.equal( valid.url );
					reqStub.getCall( 0 ).args[ 1 ].url.should.equal( valid.url );
				} );

				it( "should use the provided config `urlRoot` property if one is present", () => {
					let thisRoot = "http://www.testwebsite.com/";
					let url = "test/url";

					request( Object.assign( {}, valid, { "url": url, "urlRoot": thisRoot } ) );

					reqStub.getCall( 0 ).args[ 0 ].should.equal( `${thisRoot}${url}` );
					reqStub.getCall( 0 ).args[ 1 ].url.should.equal( `${thisRoot}${url}` );
				} );
			} );

			describe( "headers", function UrlSuite(){
				it( "should provide a Content-Type of JSON if a Content-Type is not present", () => {
					request( valid );

					reqStub.getCall( 0 ).args[ 1 ].headers.should.have.property( "Content-Type", "application/json;charset=UTF-8" );
				} );

				it( "should use the provided Content-Type if it's present", () => {
					request( Object.assign( {}, valid, { "headers": { "Content-Type": "sometext" } } ) );

					reqStub.getCall( 0 ).args[ 1 ].headers.should.have.property( "Content-Type", "sometext" );
				} );

				it( "should not add the Content-Type if the defaultContent option is set to false", () => {
					request( Object.assign( {}, valid, { "defaultContent": false } ) );

					reqStub.getCall( 0 ).args[ 1 ].headers.should.not.have.property( "Content-Type" );
				} );
			} );

			describe( "body", function UrlSuite(){
				it( "should not include any body if the request method is GET or HEAD", () => {
					request( Object.assign( {}, valid, {
						"method": "get",
						"body": { "alpha": "beta" }
					} ) );

					reqStub.getCall( 0 ).args[ 1 ].should.not.have.keys( [ "body" ] );
				} );

				it( "should JSON.stringify the body for any non-`false` or empty value of `stringifyBody`", () => {
					request( Object.assign( {}, valid, {
						"method": "post",
						"body": { "alpha": "beta" },
						"stringifyBody": "true"
					} ) );

					reqStub.getCall( 0 ).args[ 1 ].should.have.property( "body", JSON.stringify( { "alpha": "beta" } ) );

					reqStub.resetHistory();

					request( Object.assign( {}, valid, {
						"method": "post",
						"body": { "alpha": "beta" }
					} ) );

					reqStub.getCall( 0 ).args[ 1 ].should.have.property( "body", JSON.stringify( { "alpha": "beta" } ) );
				} );

				it( "should not JSON.stringify the body if the config option `stringifyBody` is exactly false", () => {
					request( Object.assign( {}, valid, {
						"method": "post",
						"body": { "alpha": "beta" },
						"stringifyBody": false
					} ) );

					reqStub.getCall( 0 ).args[ 1 ].should.have.deep.property( "body", { "alpha": "beta" } );
				} );
			} );

			it( "should mix the config object into the final object", () => {
				request( Object.assign( {}, valid, {
					"referrer": "",
					"cache": "no-store",
					"foo": "bar"
				} ) );

				reqStub.should.have.been.calledWithExactly( valid.url, {
					"url": valid.url,
					"method": "GET",
					"getter": "text",
					"failures": "native",
					"defaultContent": true,
					"headers": defaultHeaders,
					"urlRoot": "",
					"referrer": "",
					"cache": "no-store",
					"foo": "bar"
				} );
			} );

			it( "should mix the config object into the final object without the default Content-Type header", () => {
				request( Object.assign( {}, valid, {
					"referrer": "",
					"cache": "no-store",
					"foo": "bar",
					"defaultContent": false
				} ) );

				reqStub.should.have.been.calledWithExactly( valid.url, {
					"url": valid.url,
					"method": "GET",
					"getter": "text",
					"failures": "native",
					"defaultContent": false,
					"headers": {}, // defaultContent=false does not insert the JSON Content-Type
					"urlRoot": "",
					"referrer": "",
					"cache": "no-store",
					"foo": "bar"
				} );
			} );

			after( () => {
				fetchStub.restore();
				reqStub.restore();
				headersStub.restore();
			} );
		} );

		it( "should call fetch", () => {
			let fetchStub = sinon.stub( window, "fetch" );

			fetchStub.returns( Promise.resolve() );

			request( {
				"urlRoot": "",
				"url": "https://cors-test.appspot.com/test",
				"method": "GET"
			} );

			fetchStub.should.have.been.calledOnce;

			fetchStub.restore();
		} );

		describe( "when the `fetch` fails", function FailSuite(){
			var fetchStub;

			before( () => {
				fetchStub = sinon.stub( window, "fetch" );
			} );

			beforeEach( () => {
				fetchStub.reset();

				fetchStub.rejects( rejectedResponse );
			} );

			it( "should return a rejected Promise with the response as the rejection", ( done ) => {
				request( valid )
					.catch( ( response ) => {
						response.content.should.equal( "rejected" );
						done();
					} );
			} );

			after( () => {
				fetchStub.restore();
			} );
		} );

		describe( "when the `fetch` succeeds", function SuccessSuite(){
			var noop = () => { /* should never run */ };
			var fetchStub;

			before( () => {
				fetchStub = sinon.stub( window, "fetch" );
			} );

			beforeEach( () => {
				fetchStub.reset();

				fetchStub.resolves( resolvedResponse );
			} );

			it( "should return the correct POJO response to represent the successful response", ( done ) => {
				fetchStub.resolves( Object.assign( {}, resolvedResponse, {
					"headers": new Headers( { "Content-Type": "text/plain" } )
				} ) );

				request( valid )
					.then(
						( response ) => {
							response.should.have.all.keys( [
								"headers",
								"ok",
								"redirected",
								"status",
								"statusText",
								"type",
								"url",
								"content"
							] );
							response.headers.should.deep.equal( {
								"content-type": "text/plain"
							} );

							done();
						},
						noop
					);
			} );

			it( "should return a Promise that resolves with the response plus the property `content` that defaults to the text() resolution value", ( done ) => {
				request( valid )
					.then(
						( response ) => {
							response.should.deep.equal( Object.assign( {}, extractedResponse, { "ok": true, "content": "resolved" } ) );
							done();
						},
						noop
					);
			} );

			it( "should return a Promise that resolves with the response plus the property `content` that has the resolution value of whatever getter is requested", ( done ) => {
				fetchStub.resolves( Object.assign(
					{},
					resolvedResponse,
					{
						json(){
							return Promise.resolve( "json" );
						}
					}
				) );

				request( Object.assign( {}, valid, { "getter": "json" } ) )
					.then(
						( response ) => {
							response.should.deep.equal( Object.assign( {}, extractedResponse, { "ok": true, "content": "json" } ) );
							done();
						},
						noop
					);
			} );

			it( "should return a Promise that resolves with the response plus the property `content` that is an empty string if the API response is empty", async () => {
				let response = Object.assign( {}, extractedResponse, { "ok": true, "content": "" } );

				fetchStub.resolves( Object.assign(
					{},
					resolvedResponse,
					{
						text(){
							return Promise.resolve();
						}
					}
				) );

				( await request( valid ) ).should.deep.equal( response );
			} );

			it( "should return a Promise that rejects with the response plus the property `content` that is the correct value if the status code is in the 400-500 range and legacy failures are enabled", ( done ) => {
				fetchStub.resolves( Object.assign(
					{},
					resolvedResponse,
					{
						"status": 404,
						text(){
							return Promise.resolve( "legacy" );
						}
					}
				) );

				request( Object.assign( {}, valid, { "failures": "legacy" } ) )
					.then(
						noop,
						( response ) => {
							response.should.deep.equal( Object.assign( {}, extractedResponse, { "status": 404, "ok": true, "content": "legacy" } ) );
							done();
						}
					);
			} );

			it( "should return a Promise that rejects with the correct completely empty POJO response plus the property `error` if the AJAX is successful but resolves to a non-object", async () => {
				fetchStub.resolves( "hello" );

				try{
					await request( valid );
				}
				catch( e ){
					e.should.deep.equal( {
						"headers": {},
						"ok": undefined,
						"redirected": undefined,
						"status": undefined,
						"statusText": undefined,
						"type": undefined,
						"url": undefined,
						"error": "No such Response method 'text'."
					} );
				}
			} );

			it( "should return a Promise that rejects with the response plus the property `error` when a junk getter is provided", ( done ) => {
				request( Object.assign( {}, valid, { "getter": "STUFF" } ) )
					.then(
						noop,
						( response ) => {
							response.should.have.all.keys( [
								...Object.keys( extractedResponse ),
								"error"
							] );
							response.error.should.equal( "No such Response method 'STUFF'." );
							done();
						}
					);
			} );

			it( "should return a Promise that rejects with the thrown error if the getter throws", ( done ) => {
				fetchStub.resolves( Object.assign(
					{},
					resolvedResponse,
					{
						text(){
							throw new Error( "alpha beta gamma delta" );
						}
					}
				) );

				request( valid )
					.then(
						noop,
						( response ) => {
							response.error.should.be.an( "error" );
							response.error.message.should.equal( "alpha beta gamma delta" );
							done();
						}
					);
			} );

			it( "should return a Promise that rejects with the response if the response isn't truthy", ( done ) => {
				fetchStub.returns( Promise.resolve() );

				request( valid )
					.then(
						noop,
						( response ) => {
							response.should.deep.equal( extractedResponse );
							done();
						}
					);
			} );

			after( () => {
				fetchStub.restore();
			} );
		} );
	} );
} );

mocha.run();
