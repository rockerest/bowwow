# bowwow

A fetching best friend.

# API

`bowwow` exports a single named function, `request`, which returns a Promise.

## `request`
```
request( options )
    .then(
        ( response ) => // The request finished,
        ( response ) => // The request failed for some reason
    );
```

or

```
var response;

try{
    response = await request( options );
}
catch( err ){
    response = err;
}
```

Note that, like the underlying `fetch` API, a `500` or `404` is not a failing request (unless you change the `failures` option - see below). You'll need to handle those successes by using the `response.status` value (a direct clone of the `fetch` `Response` status property).

### options
| name | value | default | required | description |
|------|-------|---------|----------|-------------|
| `method` | String | `undefined` | Yes | This is the request method, like `"GET"` or `"POST"`. The value here will be uppercased and used without filtering, so ensure you are passing a valid HTTP method. |
| `url` | String | `undefined` | Yes | This is the url you want to go get or send data to. You cannot use a `Request` object, due to the `urlRoot` option being concatenated to it. |
| `headers` | Object; `header:value` pairs | `{ "Content-Type": "application/json;charset=UTF-8" }` | No | A map of header names to values. Providing no headers or a headers object without the `Content-Type` property will always mix in the default `Content-Type`, unless `defaultContent` option (see below ) is specified as false. |
| `body` | Anything | `undefined` | No | This is the body you might send with a request. If your request is a GET or HEAD, `body` is completely removed from the request object as having a body is illegal in those requests. `body` is affected by the `stringifyBody` setting. |
| `stringifyBody` | Boolean | `true` | No | Whether or not to stringify the body with `JSON.stringify`. If you want to be able to send JSON blobs as your body, leave this setting alone. If you already know that your content is in the exact format that will transfer via HTTP properly, you can set this to `false` and the `body` will not be touched. |
| `urlRoot` | String | `""` | No | A string value to prepend to all requests. This is useful if you want to use `bowwow` as your underlying AJAX provider and have consuming clients use paths relative to a known, common base. For example, you could provide `{ "urlRoot": "https://my.api.company.com" }` for every request, and then at a higher level `myApiBridge.makeRequest( { "method": "get", url": "/dogs/1/bones/1/geo" } );`. This could (depending on your implementation of `makeRequest`) result in a call to `bowwow.request` with `{ "urlRoot": "https://my.api.company.com", "url": "/dogs/1/bones/1/geo", "method": "get" }`. And the final API call would be to `https://my.api.company.com/dogs/1/bones/1/geo`. |
| `getter` | String | `"text"` | No | The [Body method](https://developer.mozilla.org/en-US/docs/Web/API/Body) to use to read the response data. By default this is `text`. The body is then passed through a failsafe JSON parse function. For valid JSON responses, this will return the parsed JSON. For plain-text responses, this will return the plain text unedited. Due to this parsing, using `"blob"`, `"arrayBuffer"`, or `"formData"` for this value is undocumented, but should work. |
| `failures` | String | `"native"` | No | When to trigger a Promise rejection. By default, this handles Promises the same way the `fetch` API does. In other words, 4xx or 5xx responses are not considered promise rejections. Setting this to `"legacy"` will treat a 4xx or 5xx response as a failure. |
| `defaultContent` | Boolean | `true` | No | By default, `bowwow` provides the `application/json` Content-Type. This is useful for the majority of use-cases interacting with APIs and 3rd party services. In some situations, you may want to completely remove the `Content-Type` header (rather than override it; see `headers` above if you would like to provide your own `Content-Type`). To completely remove the `Content-Type` header: do not pass anything in that field and set this option (`defaultContent`) to false. |

### response

The response (after Promise resolution) of the `request` method is a flat clone of the `Response` object from the underlying `fetch` client.

It will have all of these keys, copied directly from the matching read-only properties of the Response object:
```
Object.keys( response );
/*
    [
        "ok",
        "redirected",
        "status",
        "statusText",
        "type",
        "url",
        "headers"*,
        "content"*
    ]
*/
```

The `headers` property is a flattened copy of the `Response.headers` value (a native `Header` object). Note that this property is **not** itself a `Headers` object, but is instead a plain object with its properties copied over.

The `response` will contain the property `content` when the response ostensibly has a body to fetch. The value of this property will vary based on the `getter` option (see above), but will contain the final value as resolved by the `getter`. If the request results in no returned body, this property will be an empty string.

When the fetch fails or if getting the body content fails (rare), the `response` will contain an `error` property containing the caught Error, or a message like "No such Response method 'foo'." if you set the `getter` option (see above) to something that doesn't exist.
